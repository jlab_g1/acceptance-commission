package com.epam.jlabg1.config;

import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@ComponentScan("com.epam.jlabg1.db")
@PropertySource("classpath:connection.properties")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DBConfig {

    @Value("${url}")
    String url;
    @Value("${name}")
    String username;
    @Value("${password}")
    String password;
    @Value("${driver}")
    String driver;

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource customPostgreSqlDataSource = new DriverManagerDataSource(url, username, password);
        customPostgreSqlDataSource.setDriverClassName(driver);
        return customPostgreSqlDataSource;
    }

    @Bean
    public JdbcTemplate jdbcTemplate(@Autowired DataSource ds) {
        return new JdbcTemplate(ds);
    }
}
