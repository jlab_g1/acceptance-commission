package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dto.CharityEventUserTransfer;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.SubjectEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;
import java.util.Optional;

public interface UserService extends UserDetailsService{
    boolean registration(UserEntity user);
    Optional<UserEntity> authorization(UserEntity user);
    List<DepartmentEntity> getDepartments();
    List<UserEntity> getEnrolledApplicatnts(DepartmentEntity department);
    List<SubjectEntity> getSubjectsFromDepartment(DepartmentEntity department);
    CharityEventUserTransfer takePartInCharity(UserEntity user, Double value);
}
