package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dto.DepartmentSubjectTransfer;
import com.epam.jlabg1.db.dto.DepartmentUserTransfer;
import com.epam.jlabg1.db.entity.*;

import java.util.List;
import java.util.Map;

public interface UserManagementService {
    UserEntity setRole(UserEntity user, Role role);
    UserEntity setApprove(UserEntity user, Boolean approve);
    DepartmentUserTransfer getAbitList(DepartmentEntity departmentEntity);
    DepartmentUserTransfer setEnteredIn(UserEntity user, DepartmentEntity department, Boolean entered_in);
    DepartmentUserTransfer addUserBonusPoints(UserEntity user, DepartmentEntity department, Integer bonusPonits);
    void cleanUserDataFromDepartmenthasUser(UserEntity user);
    void finishEnrollment();
}
