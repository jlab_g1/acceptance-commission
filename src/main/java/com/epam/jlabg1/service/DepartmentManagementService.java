package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dto.DepartmentSubjectTransfer;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.SubjectEntity;
import com.epam.jlabg1.db.entity.UserEntity;

public interface DepartmentManagementService {
    SubjectEntity addSubject(SubjectEntity subject);
    DepartmentSubjectTransfer addSubjectToDepartment(DepartmentEntity department, SubjectEntity subject, Integer minPoint);
    DepartmentSubjectTransfer changeMinPoint(DepartmentEntity department,SubjectEntity subject, Integer newMinPoint);
    void deleteSubject(SubjectEntity subject);
    void deleteSubjectFromDepartment(DepartmentEntity department, SubjectEntity subject);
    DepartmentEntity addDepartment(DepartmentEntity department);
    DepartmentEntity changeDepartment(DepartmentEntity department);
    void deleteDepartment(DepartmentEntity department);
}
