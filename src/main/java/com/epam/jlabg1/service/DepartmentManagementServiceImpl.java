package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.DepartmentDAO;
import com.epam.jlabg1.db.dao.SubjectDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.DepartmentSubjectTransfer;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.SubjectEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DepartmentManagementServiceImpl implements DepartmentManagementService {
    @Autowired
    UserDAO userDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    DepartmentDAO departmentDAO;

    @Override
    public SubjectEntity addSubject(SubjectEntity subject) {
        return subjectDAO.save(subject);
    }

    @Override
    public DepartmentSubjectTransfer addSubjectToDepartment(DepartmentEntity department, SubjectEntity subject, Integer minPoint) {
        return departmentDAO.addSubjectToDepartment(department, subject, minPoint);
    }

    @Override
    public DepartmentSubjectTransfer changeMinPoint(DepartmentEntity department, SubjectEntity subject, Integer newMinPoint) {
        return departmentDAO.updateMinPoint(department, subject, newMinPoint);
    }

    @Override
    public void deleteSubject(SubjectEntity subject) {
        subjectDAO.delete(subject);
    }

    @Override
    public void deleteSubjectFromDepartment(DepartmentEntity department, SubjectEntity subject) {
        departmentDAO.deleteSubjectFromDepartment(department, subject);
    }

    @Override
    public DepartmentEntity addDepartment(DepartmentEntity department) {
        return departmentDAO.save(department);
    }

    @Override
    public DepartmentEntity changeDepartment(DepartmentEntity department) {
        departmentDAO.update(department);
        return departmentDAO.findById(department.getId()).orElse(new DepartmentEntity());
    }

    @Override
    public void deleteDepartment(DepartmentEntity department) {
        departmentDAO.delete(department);
    }
}
