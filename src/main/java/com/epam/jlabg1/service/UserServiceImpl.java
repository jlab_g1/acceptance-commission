package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.CharityEventDAO;
import com.epam.jlabg1.db.dao.DepartmentDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.CharityEventUserTransfer;
import com.epam.jlabg1.db.entity.CharityEventEntity;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.SubjectEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
@RequiredArgsConstructor
@Value
public class UserServiceImpl implements UserService {
    UserDAO userDAO;
    DepartmentDAO departmentDAO;
    CharityEventDAO charityEventDAO;
    PasswordEncoder bCryptPasswordEncoder;

    @Override
    public boolean registration(UserEntity user) {
        if(!userDAO.findByEmail(user).isPresent()){
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            userDAO.save(user);
            return true;
        }
        return false;
    }

    @Override
    public Optional<UserEntity> authorization(UserEntity user) {
        Optional<UserEntity> base = userDAO.findByEmail(user);
        if(base.isPresent()&& user.getPassword().equals(base.get().getPassword())) return base;
        return Optional.empty();
    }

    @Override
    public List<DepartmentEntity> getDepartments() {
        return departmentDAO.findAll();
    }

    @Override
    public List<UserEntity> getEnrolledApplicatnts(DepartmentEntity department) {
        return userDAO.getEnrolledApplicatnts(department);
    }

    @Override
    public List<SubjectEntity> getSubjectsFromDepartment(DepartmentEntity department) {
        return userDAO.getSubjectsFromDepartment(department);
    }

    @Override
    public CharityEventUserTransfer takePartInCharity(UserEntity user, Double value) {
        List<CharityEventEntity> charities = charityEventDAO.findAll();
        CharityEventEntity randomCharity = charities.get(new Random().nextInt(charities.size()));
        return userDAO.takePartInCharity(user,randomCharity,value);
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<UserEntity> user = userDAO.findByEmail(email);
        return user.orElseThrow(() -> new UsernameNotFoundException("email - " + email + " was not found!"));
    }
}
