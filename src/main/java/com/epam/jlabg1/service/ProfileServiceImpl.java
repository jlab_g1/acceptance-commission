package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.AdditionalDataDAO;
import com.epam.jlabg1.db.dao.AttachmentDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.DepartmentUserTransfer;
import com.epam.jlabg1.db.dto.UserSubjectTransfer;
import com.epam.jlabg1.db.entity.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class ProfileServiceImpl implements ProfileService {
    @Autowired
    AdditionalDataDAO additionalDataDAO;
    @Autowired
    AttachmentDAO attachmentDAO;
    @Autowired
    UserDAO userDAO;

    @Override
    public AdditionalDataEntity addUserAdditionalData(UserEntity user, AdditionalDataEntity additionalData) {
        additionalData.setUserrId(user.getId());
        return additionalDataDAO.save(additionalData);
    }

    @Override
    public void deleteUserAdditionalData(AdditionalDataEntity additionalData) {
        Optional<UserEntity> user = userDAO.findById(additionalData.getUserrId());
        additionalDataDAO.delete(additionalData);
        user.ifPresent(u -> userDAO.setApproveToUser(u, false));
    }

    @Override
    public AttachmentEntity addUserAttachment(UserEntity user, AttachmentEntity attachment) {
        attachment.setUserrId(user.getId());
        return attachmentDAO.save(attachment);
    }

    @Override
    public void deleteUserAttachment(AttachmentEntity attachment) {
        Optional<UserEntity> user = userDAO.findById(attachment.getUserrId());
        attachmentDAO.delete(attachment);
        user.ifPresent(u -> userDAO.setApproveToUser(u, false));
    }

    @Override
    public AdditionalDataEntity sendChangeAdditionalDataRequest(AdditionalDataEntity additionalData) {
        Optional<UserEntity> user = userDAO.findById(additionalData.getUserrId());
        additionalDataDAO.update(additionalData);
        user.ifPresent(u -> userDAO.setApproveToUser(u, false));
        return additionalDataDAO.findById(additionalData.getId()).orElse(new AdditionalDataEntity());
    }

    @Override
    public UserSubjectTransfer addUserSubject(UserEntity user, Map<SubjectEntity, Integer> points) {
        return userDAO.addUserSubject(user, points);
    }

    @Override
    public UserSubjectTransfer getUserSubjects(UserEntity user) {
        return userDAO.getUserSubjects(user);
    }

    @Override
    public void deleteUserSubject(UserEntity user, SubjectEntity subject) {
        userDAO.setApproveToUser(user, false);
        userDAO.deleteUserSubject(user, subject);
    }

    @Override
    public UserSubjectTransfer sendChangePointsRequest(UserEntity user, Map<SubjectEntity, Integer> newPoints) {
        userDAO.setApproveToUser(user, false);
        return userDAO.changePointsRequest(user, newPoints);
    }

    @Override
    public DepartmentUserTransfer addUserDepartment(UserEntity user, DepartmentEntity department) {
        return userDAO.addUserDepartment(user, department);
    }

    @Override
    public void deleteUserDepartment(UserEntity user, DepartmentEntity department) {
        userDAO.deleteUserDepartment(user, department);
    }
}
