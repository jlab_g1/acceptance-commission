package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dto.DepartmentUserTransfer;
import com.epam.jlabg1.db.dto.UserSubjectTransfer;
import com.epam.jlabg1.db.entity.*;

import java.util.Map;

public interface ProfileService {
    AdditionalDataEntity addUserAdditionalData(UserEntity user, AdditionalDataEntity additionalData);
    void deleteUserAdditionalData(AdditionalDataEntity additionalData);
    AttachmentEntity addUserAttachment(UserEntity user, AttachmentEntity attachment);
    void deleteUserAttachment(AttachmentEntity attachment);
    AdditionalDataEntity sendChangeAdditionalDataRequest(AdditionalDataEntity additionalData);
    UserSubjectTransfer addUserSubject(UserEntity user, Map<SubjectEntity, Integer> points);
    UserSubjectTransfer getUserSubjects(UserEntity user);
    void deleteUserSubject(UserEntity user,SubjectEntity subject);
    UserSubjectTransfer sendChangePointsRequest(UserEntity user, Map<SubjectEntity, Integer> newPoints);
    DepartmentUserTransfer addUserDepartment(UserEntity user, DepartmentEntity department);
    void deleteUserDepartment(UserEntity user, DepartmentEntity department);
}
