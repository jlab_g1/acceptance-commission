package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.*;
import com.epam.jlabg1.db.entity.*;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UtilService {
    @Autowired
     SubjectDAO subjectDAO;
    @Autowired
     DepartmentDAO departmentDAO;
    @Autowired
     AdditionalDataDAO additionalDataDAO;
    @Autowired
     AttachmentDAO attachmentDAO;
    @Autowired
     UserDAO userDAO;
    @Autowired
     CharityEventDAO charityEventDAO;

    public  Optional<UserEntity> findUserById(int id) {
        return userDAO.findById(id);
    }

    public  Optional<AdditionalDataEntity> findAdditionalDataById(int id) {
        return additionalDataDAO.findById(id);
    }

    public  Optional<AttachmentEntity> findAttachmentById(int id) {
        return attachmentDAO.findById(id);
    }

    public  Optional<DepartmentEntity> findDepartmentById(int id) {
        return departmentDAO.findById(id);
    }

    public  Optional<SubjectEntity> findSubjectById(int id) {
        return subjectDAO.findById(id);
    }

    public  Optional<CharityEventEntity> findCharityEventById(int id) {
        return charityEventDAO.findById(id);
    }
}
