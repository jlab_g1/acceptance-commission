package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.DepartmentDAO;
import com.epam.jlabg1.db.dao.SubjectDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.DepartmentUserTransfer;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.Role;
import com.epam.jlabg1.db.entity.UserEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserManagementServiceImpl implements UserManagementService {

    @Autowired
    UserDAO userDAO;
    @Autowired
    SubjectDAO subjectDAO;
    @Autowired
    DepartmentDAO departmentDAO;

    @Override
    public UserEntity setRole(UserEntity user, Role role) {
        return userDAO.setUserRole(user, role);
    }

    @Override
    public UserEntity setApprove(UserEntity user, Boolean approve) {
        return userDAO.setUserApproved(user, approve);
    }

    @Override
    public DepartmentUserTransfer getAbitList(DepartmentEntity departmentEntity) {
        return userDAO.getAbitListFromDepartment(departmentEntity);
    }

    @Override
    public DepartmentUserTransfer setEnteredIn(UserEntity user, DepartmentEntity department, Boolean entered_in) {
        return userDAO.setEnteredIn(user, department, entered_in);
    }

    @Override
    public DepartmentUserTransfer addUserBonusPoints(UserEntity user, DepartmentEntity department, Integer bonusPonits) {
        return userDAO.addUserBonusPoints(user, department, bonusPonits);
    }

    @Override
    public void cleanUserDataFromDepartmenthasUser(UserEntity user) {
        userDAO.cleanUserData(user);
    }

    @Override
    public void finishEnrollment() {
    }
}
