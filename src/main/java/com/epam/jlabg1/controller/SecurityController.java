package com.epam.jlabg1.controller;

import com.epam.jlabg1.db.entity.UserEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SecurityController {

    @RequestMapping("/")
    public String getMainPage(Model model) {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (!principal.toString().equals("anonymousUser")) {
            UserEntity user = (UserEntity)principal;
            model.addAttribute("isUserAuthenticated", true);
            model.addAttribute("email", user.getUsername());
        } else {
            model.addAttribute("isUserAuthenticated", false);
        }
        return "index";
    }

    @RequestMapping("/login")
    public String getLogin(@RequestParam(value = "error", required = false) String error,
                           @RequestParam(value = "logout", required = false) String logout,
                           Model model) {
        model.addAttribute("error", error != null);
        if (logout != null) {
            return "redirect:/";
        }

        return "login";
    }


}
