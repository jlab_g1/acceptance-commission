package com.epam.jlabg1.db.dao;

import com.epam.jlabg1.db.dto.CharityEventUserTransfer;
import com.epam.jlabg1.db.dto.DepartmentUserTransfer;
import com.epam.jlabg1.db.dto.UserSubjectTransfer;
import com.epam.jlabg1.db.entity.CharityEventEntity;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.Role;
import com.epam.jlabg1.db.entity.SubjectEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.flywaydb.core.internal.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.springframework.dao.support.DataAccessUtils.singleResult;

@Repository
@FieldDefaults(level = AccessLevel.PRIVATE)
@NoArgsConstructor
public class UserDAO implements SuperDAO<UserEntity> {

    @Autowired
    JdbcTemplate jdbcTemplate;
    static final String FIND_BY_ID = "SELECT * FROM userr WHERE id=?";
    static final String SAVE = "INSERT INTO userr(email,password,role,approved,full_name) VALUES(?,?,?,?,?) RETURNING *";
    static final String UPDATE = "UPDATE userr SET email=COALESCE(?, email)," +
            " password=COALESCE(?, password), role=COALESCE(?, role)," +
            " approved=COALESCE(?, approved),full_name=COALESCE(?,full_name) WHERE id=?";

    static final String DELETE = "DELETE FROM userr WHERE id=?";
    static final String FIND_ALL = "SELECT * FROM userr";
    static final String COUNT = "SELECT COUNT(*) FROM userr";

    static final String UPDATE_APPROVED = "UPDATE userr SET approved=? WHERE id=?";
    static final String INSERT_SUBJECT_TO_USER = "INSERT INTO userr_has_subject VALUES(?,?,?);";
    static final String DELETE_SUBJECT_FROM_USER = "DELETE FROM userr_has_subject WHERE userr_id=? and subject_id=?;";
    static final String FIND_ALL_USER_SUBJECTS = "SELECT subject_id, title, point FROM userr_has_subject, subject WHERE userr_id=? AND subject_id=id;";
    static final String UPDATE_USER_POINTS = "UPDATE userr_has_subject SET point=? WHERE subject_id=? AND  userr_id=?;";
    static final String INSERT_USER_TO_DEPARTMENT = "INSERT INTO department_has_userr(userr_id, department_id) VALUES(?,?);";
    static final String DELETE_USER_FROM_DEPARTMENT = "DELETE FROM department_has_userr WHERE userr_id=? AND department_id=?;";
    static final String FIND_USER_BY_EMAIL = "SELECT * FROM userr WHERE email=?;";

    static final String SET_USER_ROLE = "UPDATE userr SET role =? WHERE id=?;";
    static final String SET_USER_ENTERED_IN = "UPDATE department_has_userr SET entered_in=? WHERE userr_id=? AND department_id=?";
    static final String GET_ALL_ABIT_FROM_DEPARTMENT =
            "SELECT userr_id, sum_of_points, entered_in, email, password, role, approved,full_name FROM department_has_userr," +
                    " userr WHERE department_id=? AND userr_id=id";
    static final String ADD_BONUS_POINTS_TO_USER = "UPDATE department_has_userr SET sum_of_points=? WHERE user_id=? AND department_id=?;";
    static final String DELETE_USER_DATA_FROM_DEPARTMENTS = "DELETE FROM department_has_userr WHERE userr_id=?;";

    static final String FIND_BY_EMAIL = "SELECT * FROM userr WHERE email=?;";
    static final String FIND_ENROLLED_APPLICANTS_FOR_DEPARTMENT =
            "SELECT email, password, role,approved,userr_id,full_name FROM department_has_userr," +
                    " userr WHERE entered_in=true AND userr_id=id AND department_id=?;";
    static final String FIND_SUBJECTS_FROM_DEPARTMENT =
            "SELECT title,subject_id FROM department_has_subject," +
                    " subject WHERE department_id=? AND subject_id=id;";
    static final String TAKE_PART_IN_CHARITY =
            "INSERT INTO charity_event_has_userr(charity_event_id,userr_id,value) VALUES(?,?,?);";
    static final String FIND_ALL_DONATORS_FOR_CHARITY =
            "SELECT email, password, role,approved,userr_id,full_name, value " +
                    "FROM charity_event_has_userr, userr WHERE  charity_event_id=?;";
    static final String ADD_USER_TO_DEPARTMENT =
            "INSERT INTO department_has_userr VALUES(?,?,?,?);";

    @Autowired
    public UserDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public UserEntity setUserRole(UserEntity user, Role role) {
        jdbcTemplate.update(SET_USER_ROLE, role.toString(), user.getId());
        return findById(user.getId()).orElse(new UserEntity());
    }

    public UserEntity setUserApproved(UserEntity user, Boolean approved){
        jdbcTemplate.update(UPDATE_APPROVED, approved, user.getId());
        return findById(user.getId()).orElse(new UserEntity());
    }

    public DepartmentUserTransfer setEnteredIn(UserEntity user, DepartmentEntity department, Boolean entered_in) {
        jdbcTemplate.update(SET_USER_ENTERED_IN, entered_in, user.getId(), department.getId());
        return getAbitListFromDepartment(department);
    }

    public DepartmentUserTransfer getAbitListFromDepartment(DepartmentEntity department) {
        Map<UserEntity, Pair<Boolean, Integer>> abiturients = new HashMap<>();
        List<Map<String, Object>> list = jdbcTemplate.queryForList(GET_ALL_ABIT_FROM_DEPARTMENT,
                department.getId());
        for (Map<String, Object> row : list) {
            UserEntity user = new UserEntity();
            user.setId((Integer) row.get("userr_id"));
            user.setEmail((String) row.get("email"));
            user.setPassword((String) row.get("password"));
            user.setRole(getRole((String) row.get("role")));
            user.setApproved((Boolean) row.get("approved"));
            user.setFullName((String)row.get("full_name"));
            Pair pair = Pair.of((Boolean) row.get("entered_in"), (Integer) row.get("sum_of_points"));
            abiturients.put(user, pair);
           // System.out.println(row.get("name"));
        }
        return new DepartmentUserTransfer(department, abiturients);
    }

    public DepartmentUserTransfer addUserBonusPoints(UserEntity user, DepartmentEntity department, Integer bonusPonits) {
        jdbcTemplate.update(ADD_BONUS_POINTS_TO_USER, bonusPonits, user.getId(), department.getId());
        return getAbitListFromDepartment(department);
    }

    public void cleanUserData(UserEntity user) {
        jdbcTemplate.update(DELETE_USER_DATA_FROM_DEPARTMENTS, user.getId());
    }

    @Override
    public Optional<UserEntity> findById(int id) {
        return Optional.ofNullable(DataAccessUtils
                .singleResult(jdbcTemplate
                        .query(FIND_BY_ID,
                                new BeanPropertyRowMapper<>(UserEntity.class),
                                id)));
    }

    public void setApproveToUser(UserEntity user, boolean isItApproved) {
        jdbcTemplate.update(UPDATE_APPROVED, isItApproved, user.getId());
    }

    public UserSubjectTransfer addUserSubject(UserEntity user, Map<SubjectEntity, Integer> points) {
        for (Map.Entry<SubjectEntity, Integer> subjectPoint : points.entrySet()) {
            jdbcTemplate.update(INSERT_SUBJECT_TO_USER,
                    user.getId(),
                    subjectPoint.getKey().getId(),
                    subjectPoint.getValue());
        }
        return UserSubjectTransfer.builder().user(user).points(points).build();
    }

    public UserSubjectTransfer getUserSubjects(UserEntity user) {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(FIND_ALL_USER_SUBJECTS, user.getId());
        Map<SubjectEntity, Integer> subjectWithPoint = new HashMap<>();
        for (Map<String, Object> row : list) {
            subjectWithPoint.put(new SubjectEntity((Integer) row.get("subject_id"), (String) row.get("title")),
                    (Integer) row.get("point"));
        }
        return UserSubjectTransfer.builder().user(user).points(subjectWithPoint).build();
    }

    public void deleteUserSubject(UserEntity user, SubjectEntity subject) {
        jdbcTemplate.update(DELETE_SUBJECT_FROM_USER, user.getId(), subject.getId());
    }

    public UserSubjectTransfer changePointsRequest(UserEntity user, Map<SubjectEntity, Integer> newPoints) {
        for (Map.Entry<SubjectEntity, Integer> row : newPoints.entrySet()) {
            jdbcTemplate.update(UPDATE_USER_POINTS, row.getValue(), row.getKey().getId(), user.getId());
        }
        return UserSubjectTransfer.builder().user(user).points(newPoints).build();
    }

    public DepartmentUserTransfer addUserDepartment(UserEntity user, DepartmentEntity department) {
        jdbcTemplate.update(INSERT_USER_TO_DEPARTMENT, user.getId(), department.getId());
        return DepartmentUserTransfer.builder().department(department).abiturients(new HashMap<>()).build();
    }

    public void deleteUserDepartment(UserEntity user, DepartmentEntity department) {
        jdbcTemplate.update(DELETE_USER_FROM_DEPARTMENT, user.getId(), department.getId());
    }

    @Override
    public UserEntity save(UserEntity user) {
        return jdbcTemplate.queryForObject(SAVE,
                new BeanPropertyRowMapper<>(UserEntity.class),
                user.getEmail(),
                user.getPassword(),
                user.getRole().name().toUpperCase(),
                user.getApproved(),
                user.getFullName());
    }

    @Override
    public void update(UserEntity user) {
        jdbcTemplate.update(UPDATE,
                user.getEmail(),
                user.getPassword(),
                user.getRole().name().toLowerCase(),
                user.getApproved(),
                user.getFullName(),
                user.getId());
    }

    @Override
    public void delete(UserEntity user) {
        jdbcTemplate.update(DELETE, user.getId());
    }

    @Override
    public List<UserEntity> findAll() {
        return jdbcTemplate.query(FIND_ALL, new BeanPropertyRowMapper<>(UserEntity.class));
    }

    @Override
    public int count() {
        return jdbcTemplate.queryForObject(COUNT, Integer.class);
    }

    public Optional<UserEntity> findByEmail(UserEntity user) {
        return findByEmail(user.getEmail());
    }
    public Optional<UserEntity> findByEmail(String email) {
        return Optional.ofNullable(singleResult(jdbcTemplate.query(FIND_BY_EMAIL,
                new BeanPropertyRowMapper<>(UserEntity.class),
                email)));
    }

    public List<UserEntity> getEnrolledApplicatnts(DepartmentEntity department) {

        List<UserEntity> users = new ArrayList<>();
        List<Map<String, Object>> list = jdbcTemplate.queryForList(FIND_ENROLLED_APPLICANTS_FOR_DEPARTMENT
                , department.getId());

        for (Map<String, Object> row:list){
            UserEntity user = new UserEntity();
            user.setId((Integer) row.get("userr_id"));
            user.setEmail((String)row.get("email"));
            user.setPassword((String)row.get("password"));
            user.setRole(getRole((String)row.get("role")));
            user.setApproved((Boolean)row.get("approved"));
            user.setFullName((String) row.get("full_name"));
            users.add(user);
        }
        return users;
    }

    private Role getRole(String role){
        Role rol = Role.USER;
        switch (role.toLowerCase()){
            case "user": rol = Role.USER; break;
            case "admin": rol = Role.ADMIN;break;
            case "abit" : rol =  Role.ABIT;break;
        }
        return rol;
    }


    public List<SubjectEntity> getSubjectsFromDepartment(DepartmentEntity department) {
        List<SubjectEntity> subjects = new ArrayList<>();
        List<Map<String, Object>> list = jdbcTemplate.queryForList(FIND_SUBJECTS_FROM_DEPARTMENT
                , department.getId());

        for (Map<String, Object> row:list){
            SubjectEntity subject = new SubjectEntity((Integer) row.get("subject_id"),
                    (String)row.get("title"));
            subjects.add(subject);
        }

        return subjects;
    }

    public CharityEventUserTransfer takePartInCharity(UserEntity donater,
                                                      CharityEventEntity randomCharity, Double value) {
        jdbcTemplate.update(TAKE_PART_IN_CHARITY,randomCharity.getId(), donater.getId(),value);

        List<Map<String, Object>> list = jdbcTemplate.queryForList(FIND_ALL_DONATORS_FOR_CHARITY
                , randomCharity.getId());

        Map<UserEntity,Integer> map = new HashMap<>();
        UserEntity user = new UserEntity();
        for (Map<String, Object> row:list){
            user.setId((Integer) row.get("userr_id"));
            user.setEmail((String)row.get("email"));
            user.setPassword((String)row.get("password"));
            user.setRole(getRole((String)row.get("role")));
            user.setApproved((Boolean)row.get("approved"));
            user.setFullName((String)row.get("full_name"));
            map.put(user,(Integer)row.get("value"));
        }
        return new CharityEventUserTransfer(randomCharity,map);
    }

    public void addUserToDepartment(DepartmentEntity department, UserEntity user,Boolean entered,Integer bonus){
        jdbcTemplate.update(ADD_USER_TO_DEPARTMENT,user.getId(), department.getId(),bonus , entered);

    }

}
