package com.epam.jlabg1.db.dao;

import com.epam.jlabg1.db.dto.DepartmentSubjectTransfer;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.SubjectEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DepartmentDAO implements SuperDAO<DepartmentEntity> {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    static final String SELECT_DEPARTMENT = "SELECT * FROM department WHERE id = ?";
    static final String INSERT_DEPARTMENT = "INSERT INTO department (title, capacity) VALUES(?,?) RETURNING *";
    static final String UPDATE_DEPARTMENT = "UPDATE department SET title=COALESCE(?, title), capacity=COALESCE(?, capacity) WHERE id=?;";
    static final String DELETE_DEPARTMENT = "DELETE FROM department WHERE id=?";
    static final String FIND_ALL_DEPARTMENTS = "SELECT * from department";
    static final String COUNT_DEPARTMENT = "SELECT COUNT(*) FROM department";

    static final String ADD_SUBJECT_TO_DEPARTMENT = "INSERT INTO department_has_subject VALUES(?, ?, ?);";
    static final String GET_ALL_SUBJECTS_FROM_DEPARTMENT = "SELECT subject_id, title, min_point FROM department_has_subject, subject WHERE department_id=? AND subject_id=id;";
    static final String UPDATE_MIN_POINT = "UPDATE department_has_subject SET min_point=COALESCE(?, min_point) WHERE department_id=? AND subject_id=?;";
    static final String DELETE_SUBJECT_FROM_DEPARTMENT = "DELETE FROM department_has_subject WHERE department_id=? AND subject_id=?;";

    @Override
    public Optional<DepartmentEntity> findById(int id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SELECT_DEPARTMENT,
                    new BeanPropertyRowMapper<>(DepartmentEntity.class),
                    id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public DepartmentEntity save(DepartmentEntity department) {
        return jdbcTemplate.queryForObject(INSERT_DEPARTMENT,
                new BeanPropertyRowMapper<>(DepartmentEntity.class),
                department.getTitle(),
                department.getCapacity());
    }

    @Override
    public void update(DepartmentEntity department) {
        jdbcTemplate.update(
                UPDATE_DEPARTMENT,
                department.getTitle(),
                department.getCapacity(),
                department.getId());
    }

    @Override
    public void delete(DepartmentEntity department) {
        jdbcTemplate.update(DELETE_DEPARTMENT, department.getId());
    }

    @Override
    public List<DepartmentEntity> findAll() {
        return jdbcTemplate.query(FIND_ALL_DEPARTMENTS, new BeanPropertyRowMapper<>(DepartmentEntity.class));
    }

    @Override
    public int count() {
        return jdbcTemplate.queryForObject(COUNT_DEPARTMENT, Integer.class);
    }

    public DepartmentSubjectTransfer addSubjectToDepartment(DepartmentEntity department, SubjectEntity subject, Integer minPoint) {
        jdbcTemplate.update(ADD_SUBJECT_TO_DEPARTMENT, department.getId(), subject.getId(), minPoint);
        return getAllSubjectsFromDepartment(department);
    }

    public DepartmentSubjectTransfer getAllSubjectsFromDepartment(DepartmentEntity department) {
        List<Map<String, Object>> list = jdbcTemplate.queryForList(GET_ALL_SUBJECTS_FROM_DEPARTMENT, department.getId());
        Map<SubjectEntity, Integer> subjects = new HashMap<>();
        for (Map<String, Object> row : list) {
            SubjectEntity subject = new SubjectEntity((Integer) row.get("subject_id"), (String)row.get("title"));
            Integer minPoints = (Integer) row.get("min_point");
            subjects.put(subject, minPoints);
        }
        return new DepartmentSubjectTransfer(department,subjects);
    }

    public DepartmentSubjectTransfer updateMinPoint(DepartmentEntity department, SubjectEntity subject, Integer newMinPoint) {
        jdbcTemplate.update(UPDATE_MIN_POINT, department.getId(), subject.getId(), newMinPoint);
        return getAllSubjectsFromDepartment(department);
    }

    public void deleteSubjectFromDepartment(DepartmentEntity department, SubjectEntity subject) {
        jdbcTemplate.update(DELETE_SUBJECT_FROM_DEPARTMENT, department.getId(), subject.getId());
    }
}