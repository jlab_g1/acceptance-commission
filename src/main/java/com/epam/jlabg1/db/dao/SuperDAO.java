package com.epam.jlabg1.db.dao;

import java.util.List;
import java.util.Optional;

public interface SuperDAO<T> {
    Optional<T> findById(int id);
    T save(T t);
    void update(T t);
    void delete(T t);
    List<T> findAll();
    int count();
}
