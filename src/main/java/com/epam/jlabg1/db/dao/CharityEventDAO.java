package com.epam.jlabg1.db.dao;

import com.epam.jlabg1.db.entity.CharityEventEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharityEventDAO implements SuperDAO<CharityEventEntity> {

    @Autowired
    JdbcTemplate jdbcTemplate;
    static final String SELECT_CHARITY_EVENT_BY_ID = "SELECT * FROM charity_event WHERE id=?";
    static final String INSERT_CHARITY_EVENT = "INSERT INTO charity_event (title, account) VALUES(?,?) RETURNING *";
    static final String UPDATE_CHARITY_EVENT = "UPDATE charity_event SET title=COALESCE(?, title), account=COALESCE(?, account) WHERE id=?";
    static final String DELETE_CHARITY_EVENT = "DELETE FROM charity_event WHERE id=?";
    static final String FIND_ALL_CHARITY_EVENT = "SELECT * FROM charity_event";
    static final String COUNT_CHARITY_EVENT = "SELECT COUNT(*) FROM charity_event";

    @Override
    public Optional<CharityEventEntity> findById(int id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SELECT_CHARITY_EVENT_BY_ID,
                    new BeanPropertyRowMapper<>(CharityEventEntity.class),
                    id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public CharityEventEntity save(CharityEventEntity charityEventEntity) {
        return jdbcTemplate.queryForObject(INSERT_CHARITY_EVENT,
                new BeanPropertyRowMapper<>(CharityEventEntity.class),
                charityEventEntity.getTitle(),
                charityEventEntity.getAccount());
    }

    @Override
    public void update(CharityEventEntity charityEventEntity) {
        jdbcTemplate.update(
                UPDATE_CHARITY_EVENT,
                charityEventEntity.getTitle(),
                charityEventEntity.getAccount(),
                charityEventEntity.getId());
    }

    @Override
    public void delete(CharityEventEntity charityEventEntity) {
        jdbcTemplate.update(DELETE_CHARITY_EVENT, charityEventEntity.getId());
    }

    @Override
    public List<CharityEventEntity> findAll() {
        return jdbcTemplate.query(FIND_ALL_CHARITY_EVENT, new BeanPropertyRowMapper<>(CharityEventEntity.class));
    }

    @Override
    public int count() {
        return jdbcTemplate.queryForObject(COUNT_CHARITY_EVENT, Integer.class);
    }
}
