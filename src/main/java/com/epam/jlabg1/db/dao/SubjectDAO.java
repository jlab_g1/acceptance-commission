package com.epam.jlabg1.db.dao;

import com.epam.jlabg1.db.entity.SubjectEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.Optional;

@Repository
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SubjectDAO implements SuperDAO<SubjectEntity> {

    @Autowired
    JdbcTemplate jdbcTemplate;

    final static String COUNT_SUBJECTS = "SELECT COUNT(*) FROM subject;";
    final static String FIND_ALL_SUBJECTS = "SELECT * FROM subject;";
    final static String SELECT_SUBJECT_BY_ID = "SELECT * FROM subject WHERE id=?";
    final static String INSERT_SUBJECT = "INSERT INTO subject(title) VALUES(?) RETURNING *";
    final static String UPDATE_SUBJECT = "UPDATE subject SET title=COALESCE(?, title) WHERE id=?;";
    final static String DELETE_SUBJECT = "DELETE FROM subject WHERE id=?;";


    @Override
    public Optional<SubjectEntity> findById(int id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SELECT_SUBJECT_BY_ID,
                    new BeanPropertyRowMapper<>(SubjectEntity.class),
                    id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public SubjectEntity save(SubjectEntity subjectEntity) {
        return jdbcTemplate.queryForObject(INSERT_SUBJECT,
                new BeanPropertyRowMapper<>(SubjectEntity.class),
                subjectEntity.getTitle());
    }

    @Override
    public void update(SubjectEntity subjectEntity) {
        jdbcTemplate.update(UPDATE_SUBJECT,
                subjectEntity.getTitle(),
                subjectEntity.getId());
    }

    @Override
    public void delete(SubjectEntity subjectEntity) {
        jdbcTemplate.update(DELETE_SUBJECT, subjectEntity.getId());
    }

    @Override
    public List<SubjectEntity> findAll() {
        return jdbcTemplate.query(FIND_ALL_SUBJECTS,
                new BeanPropertyRowMapper<>(SubjectEntity.class));
    }

    @Override
    public int count() {
        return jdbcTemplate.queryForObject(COUNT_SUBJECTS, Integer.class);
    }
}
