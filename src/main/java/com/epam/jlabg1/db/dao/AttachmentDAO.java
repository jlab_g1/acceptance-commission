package com.epam.jlabg1.db.dao;

import com.epam.jlabg1.db.entity.AttachmentEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AttachmentDAO implements SuperDAO<AttachmentEntity> {

    @Autowired
    JdbcTemplate jdbcTemplate;
    static final String SELECT_ATTACHMENT_BY_ID = "SELECT * FROM attachment WHERE id=?";
    static final String INSERT_ATTACHMENT = "INSERT INTO attachment (userr_id, img_url) VALUES(?,?) RETURNING *";
    static final String UPDATE_ATTACHMENT = "UPDATE attachment SET userr_id=COALESCE(?, userr_id), img_url=COALESCE(?, img_url) WHERE id=?";
    static final String DELETE_ATTACHMENT = "DELETE FROM attachment WHERE id=?";
    static final String FIND_ALL_ATTACHMENT = "SELECT * FROM attachment;";
    static final String COUNT_ATTACHMENT = "SELECT COUNT(*) FROM attachment";

    @Override
    public Optional<AttachmentEntity> findById(int id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SELECT_ATTACHMENT_BY_ID,
                    new BeanPropertyRowMapper<>(AttachmentEntity.class),
                    id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public AttachmentEntity save(AttachmentEntity attachmentEntity) {
        return jdbcTemplate.queryForObject(INSERT_ATTACHMENT,
                new BeanPropertyRowMapper<>(AttachmentEntity.class),
                attachmentEntity.getUserrId(),
                attachmentEntity.getImgUrl());
    }

    @Override
    public void update(AttachmentEntity attachmentEntity) {
        jdbcTemplate.update(
                UPDATE_ATTACHMENT,
                attachmentEntity.getUserrId(),
                attachmentEntity.getImgUrl(),
                attachmentEntity.getId());
    }

    @Override
    public void delete(AttachmentEntity attachmentEntity) {
        jdbcTemplate.update(DELETE_ATTACHMENT, attachmentEntity.getId());
    }

    @Override
    public List<AttachmentEntity> findAll() {
        return jdbcTemplate.query(FIND_ALL_ATTACHMENT, new BeanPropertyRowMapper<>(AttachmentEntity.class));
    }

    @Override
    public int count() {
        return jdbcTemplate.queryForObject(COUNT_ATTACHMENT, Integer.class);
    }
}
