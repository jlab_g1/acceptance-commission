package com.epam.jlabg1.db.dao;

import com.epam.jlabg1.db.entity.AdditionalDataEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdditionalDataDAO implements SuperDAO<AdditionalDataEntity> {

    @Autowired
    JdbcTemplate jdbcTemplate;
    static final String SELECT_ADDITIONAL_DATA_BY_ID = "SELECT * FROM additional_data WHERE id=?";
    static final String INSERT_ADDITIONAL_DATA = "INSERT INTO additional_data (userr_id, passport_number,certificate_number) VALUES(?,?,?) RETURNING *";
    static final String UPDATE_ADDITIONAL_DATA = "UPDATE additional_data SET userr_id=COALESCE(?, userr_id), passport_number=COALESCE(?, passport_number), certificate_number=COALESCE(?, certificate_number) WHERE id=?";
    static final String DELETE_ADDITIONAL_DATA = "DELETE FROM additional_data WHERE id=?";
    static final String FIND_ALL_ADDITIONAL_DATA = "SELECT * FROM additional_data";
    static final String COUNT_ADDITIONAL_DATA = "SELECT COUNT(*) FROM additional_data";

    @Override
    public Optional<AdditionalDataEntity> findById(int id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SELECT_ADDITIONAL_DATA_BY_ID,
                    new BeanPropertyRowMapper<>(AdditionalDataEntity.class),
                    id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public AdditionalDataEntity save(AdditionalDataEntity additionalDataEntity) {
        return jdbcTemplate.queryForObject(INSERT_ADDITIONAL_DATA,
                new BeanPropertyRowMapper<>(AdditionalDataEntity.class),
                additionalDataEntity.getUserrId(),
                additionalDataEntity.getPassportNumber(),
                additionalDataEntity.getCertificateNumber());
    }

    @Override
    public void update(AdditionalDataEntity additionalDataEntity) {
        jdbcTemplate.update(
                UPDATE_ADDITIONAL_DATA,
                additionalDataEntity.getUserrId(),
                additionalDataEntity.getPassportNumber(),
                additionalDataEntity.getCertificateNumber(),
                additionalDataEntity.getId());
    }

    @Override
    public void delete(AdditionalDataEntity additionalDataEntity) {
        jdbcTemplate.update(DELETE_ADDITIONAL_DATA, additionalDataEntity.getId());
    }

    @Override
    public List<AdditionalDataEntity> findAll() {
        return jdbcTemplate.query(FIND_ALL_ADDITIONAL_DATA, new BeanPropertyRowMapper<>(AdditionalDataEntity.class));
    }

    @Override
    public int count() {
        return jdbcTemplate.queryForObject(COUNT_ADDITIONAL_DATA, Integer.class);
    }
}
