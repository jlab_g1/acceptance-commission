package com.epam.jlabg1.db.dto;

import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.SubjectEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DepartmentSubjectTransfer {
    DepartmentEntity department;
    Map<SubjectEntity, Integer> subjectsWithPoints;
}
