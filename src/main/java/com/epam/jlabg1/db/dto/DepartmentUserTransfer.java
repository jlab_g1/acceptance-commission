package com.epam.jlabg1.db.dto;

import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.flywaydb.core.internal.util.Pair;

import java.util.Map;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DepartmentUserTransfer {
    DepartmentEntity department;
    Map<UserEntity, Pair<Boolean, Integer>> abiturients;
}
