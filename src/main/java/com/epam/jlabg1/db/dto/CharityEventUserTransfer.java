package com.epam.jlabg1.db.dto;

import com.epam.jlabg1.db.entity.CharityEventEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.Map;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharityEventUserTransfer {
    CharityEventEntity event;
    Map<UserEntity, Integer> donaters;
}
