package com.epam.jlabg1.db.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AdditionalDataEntity {
    Integer id;
    Integer userrId;
    String passportNumber;
    String certificateNumber;
}
