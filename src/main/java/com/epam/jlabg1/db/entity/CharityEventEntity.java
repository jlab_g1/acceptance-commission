package com.epam.jlabg1.db.entity;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CharityEventEntity {
    Integer id;
    String title;
    String account;
}
