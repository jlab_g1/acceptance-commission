--init
INSERT INTO userr(email, password, role, approved,full_name)
VALUES('first@epam.com', '1234', 'ADMIN', NULL, 'gjkks');
INSERT INTO userr(email, password, role, approved,full_name)
VALUES('secondepam.com', '1234', 'ABIT', true, 'gjkks');
INSERT INTO userr(email, password, role, approved,full_name)
VALUES('third@epam.com', '1234', 'ABIT', false, 'gjkks');
INSERT INTO userr(email, password, role, approved,full_name)
VALUES('fourth@epam.com', '1234', 'ABIT', false, 'gjkks');

INSERT INTO subject(title) VALUES('Math');
INSERT INTO subject(title) VALUES('English');
INSERT INTO subject(title) VALUES('Proga');
INSERT INTO subject(title) VALUES('Databases');

INSERT INTO department(title, capacity) VALUES('PIiKT', 96);
INSERT INTO department(title, capacity) VALUES ('FiOI', 150);
INSERT INTO department(title, capacity) VALUES ('IKT', 42);
INSERT INTO department(title, capacity) VALUES ('IS', 200);

INSERT INTO attachment(img_url, userr_id) VALUES ('c://photo/first@epam.com', 1);
INSERT INTO attachment(img_url, userr_id) VALUES ('c://photo/second@epam.com', 2);
INSERT INTO attachment(img_url, userr_id) VALUES ('c://photo/third@epam.com', 3);

INSERT INTO additional_data(passport_number, certificate_number , userr_id) VALUES ('1passport', '1certificate', 1);
INSERT INTO additional_data(passport_number, certificate_number , userr_id) VALUES ('2passport', '2certificate', 2);
INSERT INTO additional_data(passport_number, certificate_number , userr_id) VALUES ('3passport', '3certificate', 3);

INSERT INTO charity_event(title, account) VALUES ('Иванычу на др', '#1');
INSERT INTO charity_event(title, account) VALUES ('Лёхе на сбадьбу', '#2');
INSERT INTO charity_event(title, account) VALUES ('На корпаратив', '#3');

INSERT INTO charity_event_has_userr(charity_event_id, userr_id, value) VALUES (1, 1, 300);
INSERT INTO charity_event_has_userr(charity_event_id, userr_id, value) VALUES (2, 2, 300);

