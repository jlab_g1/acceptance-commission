CREATE TABLE IF NOT EXISTS userr (
  id SERIAL NOT NULL,
  email VARCHAR(45) UNIQUE NOT NULL,
  password VARCHAR(60) NOT NULL,
  role VARCHAR(45) NOT NULL,
  approved bool NULL,
  PRIMARY KEY (id)
);


--------------------------------------------------------
-- Table department
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS department (
  id SERIAL NOT NULL,
  title VARCHAR(45) UNIQUE NOT NULL,
  capacity INT NOT NULL,
  PRIMARY KEY (id)
);

--CREATE TYPE subject_name AS ENUM ('Math', 'English', 'Proga');

-- -----------------------------------------------------
-- Table subject
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS subject (
  id SERIAL NOT NULL,
  title VARCHAR(45) UNIQUE NOT NULL,
  --title subject_name NOT NULL,
  PRIMARY KEY (id)
);

-- -----------------------------------------------------
-- Table additional_data
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS additional_data(
  id SERIAL NOT NULL,
  passport_number VARCHAR(45) UNIQUE NOT NULL,
  certificate_number VARCHAR(45) UNIQUE NOT NULL,
  userr_id INT UNIQUE,
  PRIMARY KEY (id),
  CONSTRAINT fk_additional_data_userr1
    FOREIGN KEY (userr_id)
    REFERENCES userr (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table attachment
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS attachment (
  id SERIAL NOT NULL,
  img_url VARCHAR(45) UNIQUE NULL,
  userr_id INT NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_attachment_userr1
    FOREIGN KEY (userr_id)
    REFERENCES userr (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table department_has_userr
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS department_has_userr (
  department_id INT NOT NULL,
  userr_id INT NOT NULL,
  sum_of_points INT NOT NULL,
  entered_in bool NULL,
  PRIMARY KEY (userr_id, department_id),
  CONSTRAINT fk_userr_has_department_userr
    FOREIGN KEY (userr_id)
    REFERENCES userr (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_userr_has_department_department1
    FOREIGN KEY (department_id)
    REFERENCES department (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);

-- -----------------------------------------------------
-- Table department_has_subject
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS department_has_subject (
  department_id INT NOT NULL,
  subject_id INT NOT NULL,
  min_point INT NOT NULL,
  PRIMARY KEY (department_id, subject_id),
  CONSTRAINT fk_subject_has_department_subject1
    FOREIGN KEY (subject_id)
    REFERENCES subject (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_subject_has_department_department1
    FOREIGN KEY (department_id)
    REFERENCES department (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);


-- -----------------------------------------------------
-- Table userr_has_subject
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS userr_has_subject (
  userr_id INT NOT NULL,
  subject_id INT NOT NULL,
  point INT NOT NULL,
  PRIMARY KEY (userr_id, subject_id),
  CONSTRAINT fk_userr_has_subject_userr1
    FOREIGN KEY (userr_id)
    REFERENCES userr (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_userr_has_subject_subject1
    FOREIGN KEY (subject_id)
    REFERENCES subject (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);


--------------------------------------------------------
-- Table charity_event
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS charity_event (
  id SERIAL NOT NULL,
  title VARCHAR(45) UNIQUE NOT NULL,
  account VARCHAR(45) UNIQUE NOT NULL,
  PRIMARY KEY (id)
);

-------------------------------------------------------
-- Table charity_event_has_userr
-------------------------------------------------------
CREATE TABLE IF NOT EXISTS charity_event_has_userr (
  charity_event_id INT NOT NULL,
  userr_id INT NOT NULL,
  value INT NOT NULL,
  PRIMARY KEY (charity_event_id, userr_id),
  CONSTRAINT fk_charity_event_has_userr_userr1
    FOREIGN KEY (userr_id)
    REFERENCES userr (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT fk_charity_event_has_userr_charity_event1
    FOREIGN KEY (charity_event_id)
    REFERENCES charity_event (id)
    ON DELETE RESTRICT
    ON UPDATE CASCADE
);

