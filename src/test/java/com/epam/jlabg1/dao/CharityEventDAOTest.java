package com.epam.jlabg1.dao;

import com.epam.jlabg1.db.dao.CharityEventDAO;
import com.epam.jlabg1.db.entity.CharityEventEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class CharityEventDAOTest extends SuperDAOTest<CharityEventEntity>{
    @Autowired
    CharityEventDAO charityEventDAO;

    @Before
    public void setUp() throws Exception {
        setSuperDAO(charityEventDAO);
        CharityEventEntity entity = new CharityEventEntity();
        entity.setTitle("На корпаратив");
        entity.setAccount("#123");
        setT(entity);
    }

    @Test
    public void findById() {
        CharityEventEntity entity = new CharityEventEntity();
        entity.setTitle("На корпаративвв");
        entity.setAccount("#1234");
        setT(entity);
        CharityEventEntity savedEntity = charityEventDAO.save(entity);
        CharityEventEntity entity1 = charityEventDAO.findById(savedEntity.getId()).orElse(new CharityEventEntity());
        assertEquals(savedEntity, entity1);
    }
}