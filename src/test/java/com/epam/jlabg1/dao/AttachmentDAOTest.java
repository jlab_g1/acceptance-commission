package com.epam.jlabg1.dao;

import com.epam.jlabg1.db.dao.AttachmentDAO;
import com.epam.jlabg1.db.entity.AttachmentEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

public class AttachmentDAOTest extends SuperDAOTest<AttachmentEntity>{
    @Autowired
    AttachmentDAO attachmentDAO;

    @Before
    public void setUp(){
        setSuperDAO(attachmentDAO);
        AttachmentEntity entity = new AttachmentEntity();
        entity.setUserrId(1);
        entity.setImgUrl("test photo");
        setT(entity);
    }

    @Test
    @Transactional
    public void findById(){
        AttachmentEntity entity1 = new AttachmentEntity();
        entity1.setImgUrl("test");
        entity1.setUserrId(1);
        AttachmentEntity savedEntity = attachmentDAO.save(entity1);
        AttachmentEntity entity2 = attachmentDAO.findById(savedEntity.getId()).orElse(new AttachmentEntity());
        assertEquals(savedEntity, entity2);
    }
}