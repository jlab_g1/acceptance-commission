package com.epam.jlabg1.dao;

import com.epam.jlabg1.db.dao.SuperDAO;
import lombok.AccessLevel;
import lombok.Setter;
import lombok.SneakyThrows;
import lombok.experimental.FieldDefaults;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@FieldDefaults(level = AccessLevel.PRIVATE)
public abstract class SuperDAOTest<T> {
    @Setter
    SuperDAO<T> superDAO;
    @Setter
    T t;

    @Test
    @Transactional
    public void save() {
        long countBeforeSave = superDAO.count();
        assertNotNull(superDAO.save(t));
        assertEquals(countBeforeSave+1, superDAO.count());
    }

    @Test
    @Transactional
    public void update() {
        long countBeforeUpdate = superDAO.count();
        superDAO.update(t);
        assertEquals(countBeforeUpdate, superDAO.count());
    }

    @Test
    public void findAll() {
        assertNotNull(superDAO.findAll());
    }
}
