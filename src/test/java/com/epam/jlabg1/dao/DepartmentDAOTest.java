package com.epam.jlabg1.dao;

import com.epam.jlabg1.db.dao.DepartmentDAO;
import com.epam.jlabg1.db.dao.SubjectDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.DepartmentSubjectTransfer;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.SubjectEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

public class DepartmentDAOTest extends SuperDAOTest<DepartmentEntity> {

    @Autowired
    DepartmentDAO departmentDAO;

    @Autowired
    UserDAO userDAO;

    @Autowired
    SubjectDAO subjectDAO;

    @Before
    public void setUp() {
        setSuperDAO(departmentDAO);
        setT(new DepartmentEntity(5, "FIIKT", 90));
    }

    @Test
    @Transactional
    public void addSubjectToDepartment() {
        List<SubjectEntity> subjects = subjectDAO.findAll();
        List<DepartmentEntity> departments = departmentDAO.findAll();
        DepartmentEntity department = departments.get(new Random().nextInt(departments.size()));

        DepartmentSubjectTransfer dto = departmentDAO.getAllSubjectsFromDepartment(department);
        assertEquals(dto.getSubjectsWithPoints().size(), 0);
        for (SubjectEntity subject : subjects)
             dto =  departmentDAO.addSubjectToDepartment(department, subject, 60);

        assertEquals(dto.getDepartment(), department);
        assertEquals(dto.getSubjectsWithPoints().size(), subjects.size());
    }

    @Test
    @Transactional
    public void deleteSubjectFromDepartment() {
        List<SubjectEntity> subjects = subjectDAO.findAll();
        SubjectEntity subject = subjects.get(new Random().nextInt(subjects.size()));

        List<DepartmentEntity> departments = departmentDAO.findAll();
        DepartmentEntity department = departments.get(new Random().nextInt(departments.size()));

        departmentDAO.addSubjectToDepartment(department, subject, 60);
        departmentDAO.deleteSubjectFromDepartment(department, subject);
        departmentDAO.addSubjectToDepartment(department, subject, 60);
    }
}