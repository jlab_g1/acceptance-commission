package com.epam.jlabg1.dao;

import com.epam.jlabg1.dao.SuperDAOTest;
import com.epam.jlabg1.db.dao.AdditionalDataDAO;
import com.epam.jlabg1.db.entity.AdditionalDataEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;

public class AdditionalDataDAOTest extends SuperDAOTest<AdditionalDataEntity> {
    @Autowired
    AdditionalDataDAO additionalDataDAO;

    @Before
    public void setUp(){
        setSuperDAO(additionalDataDAO);
        AdditionalDataEntity additionalDataEntity = new AdditionalDataEntity();
        additionalDataEntity.setPassportNumber("#1");
        additionalDataEntity.setUserrId(1);
        additionalDataEntity.setCertificateNumber("#2");
        setT(additionalDataEntity);
    }

    @Test
    @Transactional
    public void findById() {
        AdditionalDataEntity entity = new AdditionalDataEntity();
        entity.setPassportNumber("#test");
        entity.setUserrId(2);
        entity.setCertificateNumber("#test");
        AdditionalDataEntity savedEntity = additionalDataDAO.save(entity);
        AdditionalDataEntity entity1 = additionalDataDAO.findById(savedEntity.getId()).orElse(new AdditionalDataEntity());
        assertEquals(savedEntity, entity1);
    }
}