package com.epam.jlabg1.dao;

import com.epam.jlabg1.db.dao.CharityEventDAO;
import com.epam.jlabg1.db.dao.DepartmentDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.CharityEventUserTransfer;
import com.epam.jlabg1.db.dto.DepartmentUserTransfer;
import com.epam.jlabg1.db.entity.CharityEventEntity;
import com.epam.jlabg1.db.entity.DepartmentEntity;
import com.epam.jlabg1.db.entity.Role;
import com.epam.jlabg1.db.entity.UserEntity;
import com.epam.jlabg1.service.DepartmentManagementServiceImpl;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDAOTest extends SuperDAOTest<UserEntity> {

    @Autowired
    UserDAO userDAO;
    @Autowired
    CharityEventDAO charityEventDAO;
    @Autowired
    DepartmentDAO departmentDAO;
    @Autowired
    DepartmentManagementServiceImpl departmentManagement;

    @Before
    public void setUp() {
        setSuperDAO(userDAO);
        UserEntity u1 = new UserEntity();
        u1.setPassword("1234");
        u1.setApproved(null);
        u1.setEmail("first1@epam.com");
        u1.setRole(Role.ADMIN);
        u1.setFullName("random name");
        setT(u1);
    }


    @Test
    @Transactional
    public void findById() {
        UserEntity u1 = new UserEntity();
        u1.setPassword("qwerty");
        u1.setApproved(true);
        u1.setEmail("user666@com");
        u1.setRole(Role.USER);
        u1.setFullName("Bob Vim");
        u1 = userDAO.save(u1);
        UserEntity u2 = userDAO.findById(u1.getId()).orElse(new UserEntity());
        assertEquals(u1, u2);
    }

    @Test
    @Transactional
    public void delete() {
        int start = userDAO.count();
        UserEntity u1 = new UserEntity();
        u1.setPassword("qwerty");
        u1.setApproved(true);
        u1.setEmail("user666@com");
        u1.setRole(Role.USER);
        u1.setFullName("Mell Gibson");
        u1 = userDAO.save(u1);
        assertEquals(start + 1, userDAO.count());
        userDAO.delete(u1);
        assertEquals(start, userDAO.count());
    }

    @Test
    @Transactional
    public void findByEmail(){
        assertTrue(userDAO.findByEmail("first@epam.com").isPresent());
        assertFalse(userDAO.findByEmail("user666@com").isPresent());
    }

    @Test
    @Transactional
    public void getEnrolledApplicatnts(){
        UserEntity u1 = new UserEntity();
        u1.setId(1);
        DepartmentEntity dep = new DepartmentEntity();
        dep.setId(1);
        userDAO.addUserToDepartment(dep,u1,true,10);
        assertFalse(userDAO.getEnrolledApplicatnts(dep).isEmpty());
    }

    @Test
    @Transactional
    public void takePartInCharity(){

        ;
        CharityEventUserTransfer transfer = userDAO.takePartInCharity(userDAO.findById(1).get(),
                charityEventDAO.save(new CharityEventEntity(0,"red","777") ),
                34.5);
        assertFalse(transfer.getDonaters().isEmpty());
        assertTrue(transfer.getEvent().getAccount().equals("777"));
    }

    @Test
    @Transactional
    public void setUserRole() {
        Role role = Role.ADMIN;
        UserEntity u1 = new UserEntity();
        u1.setPassword("qwerty");
        u1.setApproved(true);
        u1.setEmail("user666@com");
        u1.setRole(Role.USER);
        u1.setFullName("May Vong");
        u1 = userDAO.save(u1);
        userDAO.setUserRole(u1, role);
        assertEquals(role, userDAO.findById(u1.getId()).orElse(new UserEntity()).getRole());
    }

    @Test
    @Transactional
    public  void setUserApproved(){
        UserEntity u1 = new UserEntity();
        u1.setPassword("qwerty");
        u1.setApproved(false);
        u1.setEmail("user666@com");
        u1.setRole(Role.USER);
        u1.setFullName("Vo vo");
        u1 = userDAO.save(u1);
        userDAO.setUserApproved(u1, true);
        assertEquals(true, userDAO.findById(u1.getId()).orElse(new UserEntity()).getApproved());
    }

    @Test
    @Transactional
    public void setEnteredIn(){
        UserEntity u1 = new UserEntity();
        u1.setPassword("qwerty");
        u1.setApproved(false);
        u1.setEmail("user666@com");
        u1.setRole(Role.USER);
        u1.setFullName("bla bla");
        u1= userDAO.save(u1);
       DepartmentUserTransfer departmentTransfer = userDAO.
               setEnteredIn(u1,departmentDAO.findById(1).get(),true);
       assertTrue(departmentTransfer.getAbiturients().isEmpty());
    }
}