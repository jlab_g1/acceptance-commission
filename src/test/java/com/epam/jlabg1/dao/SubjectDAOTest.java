package com.epam.jlabg1.dao;

import com.epam.jlabg1.db.dao.SubjectDAO;
import com.epam.jlabg1.db.entity.SubjectEntity;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class SubjectDAOTest extends SuperDAOTest<SubjectEntity> {

    @Autowired
    SubjectDAO subjectDAO;

    @Before
    public void setUp() {
        setSuperDAO(subjectDAO);
        setT(new SubjectEntity(1, "Math"));
    }

    @Test
    public void findById() {
        SubjectEntity t1 = subjectDAO.findById(1).orElse(new SubjectEntity());
        assertEquals("Math", t1.getTitle());
        assertFalse(subjectDAO.findById(10).isPresent());
    }
}