package com.epam.jlabg1.config;

import org.springframework.boot.autoconfigure.flyway.FlywayMigrationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class FWTestConfig {
    @Bean
    public FlywayMigrationStrategy flywayMigrationStrategy() {
        return fl -> {
            fl.clean();
            fl.migrate();
        };
    }
}
