package com.epam.jlabg1;


import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.Assert.assertEquals;

public class BCryptTest {
    @Test
    public void test(){
        BCryptPasswordEncoder b = new BCryptPasswordEncoder();
        String password = b.encode("1234");
        String passwor2 = b.encode("1234");
        assertEquals(passwor2, password);
    }
}
