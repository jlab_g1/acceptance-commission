package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UtilServiceTest {
    @Autowired
    UserDAO userDAO;
    @Autowired
    UtilService utilService;
    @Test
    public void findUserById() {
        Optional<UserEntity> userById = utilService.findUserById(1);
        Optional<UserEntity> byId = userDAO.findById(1);
        assertEquals(userById.get(), byId.get());
    }
}