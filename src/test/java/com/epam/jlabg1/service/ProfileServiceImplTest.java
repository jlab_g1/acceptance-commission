package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.AdditionalDataDAO;
import com.epam.jlabg1.db.dao.SubjectDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.UserSubjectTransfer;
import com.epam.jlabg1.db.entity.AdditionalDataEntity;
import com.epam.jlabg1.db.entity.Role;
import com.epam.jlabg1.db.entity.SubjectEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.w3c.dom.UserDataHandler;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class ProfileServiceImplTest {
    @Autowired
    ProfileService profileService;
    @Autowired
    UserDAO userDAO;
    @Autowired
    AdditionalDataDAO additionalDataDAO;
    @Autowired
    SubjectDAO subjectDAO;

    @Test
    public void addUserAdditionalData() {
        UserEntity user = userDAO.save(UserEntity.builder().email("hello@mail.com").password("qwerty").role(Role.ABIT).build());
        AdditionalDataEntity additional = profileService.addUserAdditionalData(user,
                AdditionalDataEntity.builder()
                        .passportNumber("#111")
                        .certificateNumber("#222")
                        .build());
        AdditionalDataEntity additionalDataEntity = additionalDataDAO.findById(additional.getId()).get();
        assertEquals(additional, additionalDataEntity);
    }

    @Test
    public void deleteUserAdditionalData() {

        AdditionalDataEntity additionalDataEntity = AdditionalDataEntity.builder().certificateNumber("1").passportNumber("2").build();
        UserEntity user = userDAO.findById(1).get();
        profileService.addUserAdditionalData(user, additionalDataEntity);
        profileService.deleteUserAdditionalData(additionalDataEntity);
    }

    @Test
    public void sendChangeAdditionalDataRequest() {
        AdditionalDataEntity additionalDataEntity = AdditionalDataEntity.builder()
                .id(1)
                .userrId(1)
                .passportNumber("test passport").build();
        AdditionalDataEntity updatedAdditionalDataEntity = profileService.sendChangeAdditionalDataRequest(additionalDataEntity);
        assertEquals(additionalDataEntity.getPassportNumber(), updatedAdditionalDataEntity.getPassportNumber());
    }

    @Test
    public void getUserSubjects() {
        Map<SubjectEntity, Integer> map = new HashMap<>();
        map.put(subjectDAO.findById(1).get(), 70);
        map.put(subjectDAO.findById(2).get(), 80);
        UserEntity userEntity = userDAO.findById(1).get();
        UserSubjectTransfer userSubject1 = profileService.addUserSubject(userEntity, map);
        UserSubjectTransfer userSubject2 = profileService.getUserSubjects(userEntity);
        System.out.println(userSubject1);
        System.out.println(userSubject2);
        assertEquals(userSubject1, userSubject2);
    }

}