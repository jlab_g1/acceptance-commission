package com.epam.jlabg1.service;

import com.epam.jlabg1.db.dao.CharityEventDAO;
import com.epam.jlabg1.db.dao.DepartmentDAO;
import com.epam.jlabg1.db.dao.UserDAO;
import com.epam.jlabg1.db.dto.CharityEventUserTransfer;
import com.epam.jlabg1.db.entity.CharityEventEntity;
import com.epam.jlabg1.db.entity.UserEntity;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserServiceImplTest {

    @Autowired
    UserServiceImpl userService;
    @Autowired
    UserDAO userDAO;
    @Autowired
    DepartmentDAO departmentDAO;
    @Autowired
    CharityEventDAO charityEventDAO;

    @Test
    @Transactional
    public void registration() {
        UserEntity u1 = userDAO.findById(1).get();
        assertFalse(userService.registration(u1));
        u1.setEmail("new@.com");
        assertTrue(userService.registration(u1));
    }

    @Test
    @Transactional
    public void authorization() {
        UserEntity u1 = userDAO.findById(1).get();
        assertTrue(userService.authorization(u1).isPresent());
        u1.setEmail("new@.com");
        assertFalse(userService.authorization(u1).isPresent());
    }

    @Test
    @Transactional
    public void getDepartments() {
        assertFalse(userService.getDepartments().isEmpty());
    }

    @Test
    @Transactional
    public void getEnrolledApplicatnts() {
        assertTrue(userService.getEnrolledApplicatnts(departmentDAO.findById(1).get()).isEmpty());
    }

    @Test
    @Transactional
    public void getSubjectsFromDepartment() {
        assertTrue(userService.getSubjectsFromDepartment(departmentDAO.findById(1).get()).isEmpty());
    }

    @Test
    @Transactional
    public void takePartInCharity() {
        CharityEventUserTransfer charityTransfer = userService.takePartInCharity(userDAO.findById(1).get(),
                30.);
        List<CharityEventEntity> charities  = charityEventDAO.findAll();
        assertTrue(charities.contains(charityTransfer.getEvent()));
    }
}